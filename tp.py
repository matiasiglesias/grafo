visitar_nulo = lambda a,b,c,d: True
heuristica_nula = lambda actual,destino: 0
import random
import heapq
import csv
INFINITO = 9999
RUTA_ENTRADA = "/home/matias/Escritorio/grafo/marvel.pjk"

def max_freq(ady,labels):
    """Dado un diccionario personaje:label y otro con los adyacentes de un
    personje , devuelve el label demayor frecuencia entre los adyacentes"""
    dic_freq = {}
    for key in ady:
        label = labels[key]
        valor = dic_freq.get(label,0)
        valor +=1
        dic_freq[label]= valor
    lista_de_freq = list(dic_freq.values())
    lista_de_freq.sort
    mayor_freq = lista_de_freq[0]
    for label in dic_freq:
        if dic_freq[label] == mayor_freq:
            return label
    
def labelss(vertices):
    """Dado un diccionario donde las claves son los vertices, le asigna 
    un label distinto a cada vertice y devuelve la infromacion en un 
    diccionario vertice:label"""
    labels = {}
    cont = 0
    for v in vertices:
        labels[v] = cont
        cont+=1
    return labels		 

def swap(lista,n,i):
	"""Dada una lista y dos posiciones intecambia los valores asociados
	a las posiciones"""
	aux = lista[n]
	lista[n]=lista[i]
	lista[i]=lista[n]
	    
def recolectar_comunidad(labels):
	"""Dado un diccionario vertice:label devuelve un diccionario 
	label:[personajes con dicha label(comunidades)]"""
	comunidades = {}
	label = list(labels.values())
	for x in label:
		comunidades[x] = []
	for vertice in labels:
		comunidades[labels[vertice]].append(vertice)
	return comunidades		
    
def	orden_aleatorio(lista):
	"""Dada una lista , "mezcla" los valores aleatoriamente"""
	largo = len(lista)
	for i in range (largo-1):
		swap(lista,i,random.randrange(0,largo -1))


class Grafo(object):
    '''Clase que representa un grafo. El grafo puede ser dirigido, o no, y puede no indicarsele peso a las aristas
    (se comportara como peso = 1). Implementado como "diccionario de diccionarios"'''
    
    def __init__(self, es_dirigido = False):
        '''Crea el grafo. El parametro 'es_dirigido' indica si sera dirigido, o no.'''
        self.vertices = {}
        self.cantidad = 0
        self.n_aristas = 0
    
    def __len__(self):
        '''Devuelve la cantidad de vertices del grafo'''
        return self.cantidad
    
    def len_aristas(self):
        """ devuelve la cantidad de aristas dentro del grafo"""
        return self.n_aristas
    
    def densidad(self):
        """devuelve el numero que representa la densidad del grafo"""
        n_vertices = self.cantidad
        cant_max_aristas = (n_vertices*(n_vertices - 1))/2
        return self.n_aristas / cant_max_aristas
    
    def __iter__(self):
        '''Devuelve un iterador de vertices, sin ningun tipo de relacion entre los consecutivos'''
        raise NotImplementedError()
        
    def keys(self):
        '''Devuelve una lista de identificadores de vertices. Iterar sobre ellos es equivalente a iterar sobre el grafo.'''
        claves = list(self.vertices.keys()) 
        return claves
    def __getitem__(self, id):
        '''Devuelve el valor del vertice asociado, del identificador indicado. Si no existe el identificador en el grafo, lanzara KeyError.'''
        vertice = self.vertices.get(id,"no exite")
        if type(vertice) == str:
            raise KeyError("el vertice no existe")
        return vertice[0]
    
    def setitem(self, id, valor):
        '''Agrega un nuevo vertice con el par <id, valor> indicado. ID debe ser de identificador unico del vertice.
        En caso que el identificador ya se encuentre asociado a un vertice, se actualizara el valor.
        '''
        self.vertices[id]=[valor,{}]
        self.cantidad +=1
    
    def delitem(self, id):
        '''Elimina el vertice del grafo. Si no existe el identificador en el grafo, lanzara KeyError.
        Borra tambien todas las aristas que salian y entraban al vertice en cuestion.
        '''
        if id not in self.vertices:
            raise KeyError
        self.vertices.pop(id)
        self.cantidad -=1
        
    def contains(self, id):
        ''' Determina si el grafo contiene un vertice con el identificador indicado.'''
        return id in self.vertices
		
    def agregar_arista(self, desde, hasta, peso = 1):
        '''Agrega una arista que conecta los vertices indicados. Parametros:
            - desde y hasta: identificadores de vertices dentro del grafo. Si alguno de estos no existe dentro del grafo, lanzara KeyError.
            - Peso: valor de peso que toma la conexion. Si no se indica, valdra 1.
            Si el grafo es no-dirigido, tambien agregara la arista reciproca.
        '''
        personaje = self.vertices[desde]
        aristas = personaje[1]
        aristas[hasta] = peso
        personaje = self.vertices[hasta]
        aristas = personaje[1]
        aristas[desde] = peso
        self.n_aristas +=1
        
        
    def borrar_arista(self, desde, hasta):
        '''Borra una arista que conecta los vertices indicados. Parametros:
            - desde y hasta: identificadores de vertices dentro del grafo. Si alguno de estos no existe dentro del grafo, lanzara KeyError.
           En caso de no existir la arista, se lanzara ValueError.
        '''
        if desde not in self.vertices:
            raise KeyError
        personaje = self.vertices[desde]
        aristas = personaje[1]
        if hasta not in aristas:
            raise ValueError
        aristas.pop(hasta)
        """no hace falta comprobar que existe porque ya fue tratadoarriba"""
        personaje = self.vertices[hasta]
        aristas = personaje[1]
        if desde not in aristas:
            aristas.pop(desde)	
        self.n_aristas -=1
        
    def obtener_peso_arista(self, desde, hasta):
        '''Obtiene el peso de la arista que va desde el vertice 'desde', hasta el vertice 'hasta'. Parametros:
            - desde y hasta: identificadores de vertices dentro del grafo. Si alguno de estos no existe dentro del grafo, lanzara KeyError.
            En caso de no existir la union consultada, se devuelve None.
        '''
        if desde not in self.vertices or hasta not in self.vertices:
            raise KeyError
        personaje = self.vertices[desde]
        aristas = personaje[1]
        if hasta not in aristas:
            return None
        return aristas[hasta]
    
    def adyacentes(self, id):
        '''Devuelve una lista con los vertices (identificadores) adyacentes al indicado. Si no existe el vertice, se lanzara KeyError'''
        if id not in self.vertices:
            raise KeyError
        personaje = self.vertices[id]
        aristas = personaje[1]
        adyacentes = list(aristas.keys())
        return adyacentes
     
    def vertices_aleatorios(pesos):
        #pesos es un diccionario de pesos, clave vertices vecino, valor el peso.
        #acomodar a implementacion 
        total = sum(pesos.values())
        rand = random.uniform(0, total)
        acum = 0
        for vertice , peso_arista in pesos.item():
            if acum + peso_arista >= rand:
                return vertice
            acum += peso_arista	
				
			
    #def bfs(self, visitar = visitar_nulo, extra = None, inicio=None):
    def bfs(self,id):
        '''Realiza un recorrido BFS dentro del grafo, aplicando la funcion pasada por parametro en cada vertice visitado.
        Parametros:
            - visitar: una funcion cuya firma sea del tipo: 
                    visitar(v, padre, orden, extra) -> Boolean
                    Donde 'v' es el identificador del vertice actual, 
                    'padre' el diccionario de padres actualizado hasta el momento,
                    'orden' el diccionario de ordenes del recorrido actualizado hasta el momento, y 
                    'extra' un parametro extra que puede utilizar la funcion (cualquier elemento adicional que pueda servirle a la funcion a aplicar). 
                    La funcion aplicar devuelve True si se quiere continuar iterando, False en caso contrario.
            - extra: el parametro extra que se le pasara a la funcion 'visitar'
            - inicio: identificador del vertice que se usa como inicio. Si se indica un vertice, el recorrido se comenzara en dicho vertice, 
            y solo se seguira hasta donde se pueda (no se seguira con los vertices que falten visitar)
        Salida:
            Tupla (padre, orden), donde :
                - 'padre' es un diccionario que indica para un identificador, cual es el identificador del vertice padre en el recorrido BFS (None si es el inicio)
                - 'orden' es un diccionario que indica para un identificador, cual es su orden en el recorrido BFS
        '''
        blanco = 0
        gris = 1
        negro = 2
        heap = []
        distancia = {}
        padres = {}
        estado={}
        vertices = self.vertices
        for x in vertices:
            distancia[x] = 0
            estado[x] = gris
            if x != id:
                distancia[x] = INFINITO
                estado[x] = blanco
            padres[x]=None
            
        heapq.heappush(heap,[distancia[id],id])    
        while len(heap)!=0:
            item = heapq.heappop(heap)
            
            adyacentes = vertices[item[1]][1]
            for personaje in adyacentes:
                if estado[personaje] == blanco:
                    estado[personaje] = gris 
                    distancia[personaje] = distancia[item[1]] + 1
                    padres[personaje] = item[1]
                    heapq.heappush(heap,[distancia[personaje],personaje]) 
            estado[personaje] = negro      
        return distancia    
        
    def random_walk(self, largo, origen = None, pesado = False):
        ''' Devuelve una lista con un recorrido aleatorio de grafo.
            Parametros:
                - largo: El largo del recorrido a realizar
                - origen: Vertice (id) por el que se debe comenzar el recorrido. Si origen = None, se comenzara por un vertice al azar.
                - pesado: indica si se tienen en cuenta los pesos de las aristas para determinar las probabilidades de movernos de un vertice a uno de sus vecinos (False = todo equiprobable). 
            Devuelve:
                Una lista con los vertices (ids) recorridos, en el orden del recorrido. 
        '''
        recorrido = []
        personajes = self.vertices
        aristas = personajes[origen]
        adyacentes = aristas[1]
        for x in range (largo):
            camino_al_azar = random.randrange(1,6411)
            #ojo si esta vacio(no teine adyacentes
            while camino_al_azar not in adyacentes:
                camino_al_azar = random.randrange(1,6411)#ver si combiene poner stpes
            recorrido.append(camino_al_azar)
            aristas = personajes[camino_al_azar]
            adyacentes = aristas[1]	
        return recorrido	
        
    def dijkstra (self, id):
        heap = []
        distancia = {}
        padres = {}
        vertices = self.vertices
        for x in vertices:
            distancia[x] = 0
            if x != id:
                distancia[x] = INFINITO
            padres[x]=None
            heapq.heappush(heap,[distancia[x],x])
        while len(heap)!=0:
            item = heapq.heappop(heap)
            adyacentes = vertices[item[1]][1]
            for personaje in adyacentes:
                if distancia[item[1]] + 1/adyacentes[personaje] < distancia[personaje]:###
                    distancia[personaje] = distancia[item[1]] + 1/adyacentes[personaje]###
                    padres[personaje] = item[1]
                    actualizar_linear(heap,personaje,distancia[personaje])
                    heapq.heapify(heap)
        return distancia , padres	    

    def label_propagation(self):
        """Devuelve un  diccionario cuyo valores son listas que representa
        comunidades dentro de la red"""
        labels = labelss(self.vertices)
        orden = list(self.vertices.keys())
        orden_aleatorio(orden)
        #cuantas veces??
        for x in range(5):
            for vertice in orden:
                labels[vertice] = max_freq(self.vertices[vertice][1],labels)
            orden_aleatorio(orden)	
		
        comunidades = recolectar_comunidad(labels) #dic label:lista (comunidad)
        return comunidades		

    def promedio_del_grado(self):
        """Devuelve el premedio del grado de todos los vertices dentro del 
        grafo"""
        grado = 0
        for vertice in self.vertices:
            adyacentes = self.vertices[vertice][1]
            for adyacente in adyacentes:
                grado += 1
        return grado / self.cantidad		 
	
		 
def actualizar_linear(lista , id , distancia):
	for x in lista:
		if x[0] == id:
			x[1] = distancia
			break
			



def carga_de_datos(grafo,linea,indicador):
	"""Recibe por parametro una linea de archivo , un indicador (indica
	como debe ser tratada la linea) y el grafo. Carga dentro del grafo la 
	informacion de la linea"""
	if indicador == 1:
		grafo.setitem(int(linea[0]),linea[1])
	elif indicador == 2:
		grafo.agregar_arista(int(linea[0]),int(linea[1]),int(linea[2])) 
						 



def gestor_de_archivo(grafo):
	"""procesa el archivo de entrada completando asi el grafo con la 
	informacion del archivo"""
	with open (RUTA_ENTRADA) as archivo_entrada:
		archivo_entrada=csv.reader(archivo_entrada,delimiter=' ')
		encabezado=next(archivo_entrada)
		linea=next(archivo_entrada,None)
		indicador = 1
		traductor = {}
		while linea:
			if len(linea)==1:#otro encabezado(paso a aristas
				indicador = 2
			else:
				if indicador ==1:
					traductor[linea[1]] = int(linea[0])	
				carga_de_datos(grafo , linea , indicador)
			linea=next(archivo_entrada,None)		 
	return traductor
	
def mostrar_opciones():
	"""imprime el menu de opciones"""
	print("""Bienvenido a la Marvel Red Social
	ELIJA UNA OPCION:
	1-Similares
	2-Recomendar
	3-Camino
	4-Centralidad
	5-Distancias
	6-Estadistica
	7-Comunidades
	8-Salir""")

def verificar(opcion):
	"""Dado el numero de opcion elejida, verifica que la opcion elejida 
	sea correcta"""
	try:
		opcion = int(opcion)
	except:
		return False
	if opcion > 8 or opcion < 1:
		return False
	return True	 		

def pedir_personaje(traductor):
	"""Dado un diccionario que traduce nombre a id , pide un personaje 
	al usuario y corrobara que sea correcto. Devuelve el id asociado al 
	personaje ingresado"""
	nombre = input("Ingrese el nombre ")
	while nombre.upper() not in traductor:
		 nombre = input("Personaje no encontrado.Ingrese el nombre ")
	return traductor[nombre.upper()]	 

def menu():
	"""menu principal de la red"""
	red = Grafo()			
	traductor = gestor_de_archivo(red)
	mostrar_opciones()
	while True:
		opcion = input()
		while True:
			if verificar(opcion):
				break
			opcion = input("Hubo un error,ingrese otra vez la opcion ")
		if opcion == "1":
			personaje = pedir_personaje(traductor)
			similares(red,personaje)
		if opcion == "2":
			personaje = pedir_personaje(traductor)
			recomendar(red,personaje)
		if opcion == "3":
			personaje1 = pedir_personaje(traductor)
			personaje2 = pedir_personaje(traductor)
			camino(red,personaje1,personaje2)
		if opcion == "4":
			print("OK")
		if opcion == "5":
			personaje = pedir_personaje(traductor)
			distancias(red,personaje)
		if opcion == "6":
			estadisticas(red)
		if opcion == "7":
			comunidades(red)
		if opcion == "8":
			break	
		s_o_n = input("Quiere salir? [S/N] ").upper()
		if	s_o_n == "S":
			break
		mostrar_opciones()	

def frecuencias(caminos,dic):
	for camino in caminos:
		for personaje in camino:
			freq = dic.get(personaje,0)
			freq +=1
			dic[personaje] = freq				
	frec = list(dic.values())
	frec.sort()
	return frec

def mayor_freq(caminos,pto_de_salida):
	dic = {}
	frec = frecuencias(caminos,dic)
	mas_frec = []
	ultimo = len(frec) -1
	for x in range(10):
		for x in dic:
			if dic[x] == frec[ultimo] :
				if x not in mas_frec:
					mas_frec.append(x)
					break
		ultimo -= 1					
	return mas_frec				

def caminos_random(grafo,personaje):
	"""Dado un personaje devuelve una lista con un "camino" random iniciado
	desde el personaje . Un camino son las id´s de cada personaje por el
	que se paso"""
	caminos = []
	for x in range(10):
		camino =grafo.random_walk(1000,personaje)
		caminos.append(camino)
	return caminos		

def similares(grafo,personaje):
	caminos=caminos_random(grafo,personaje)
	similares = mayor_freq(caminos,personaje)
	for x in similares:
		print(grafo.vertices[x][0])
		
def a_recomendar(grafo,frec,dic,personaje):
	recomendables = []
	ultimo = len(frec) -1
	adyacentes= grafo.vertices[personaje][1]
	while len(recomendables) != 10:
		for x in dic:
			if dic[x] == frec[ultimo] :
				if x not in recomendables and x not in adyacentes:
					recomendables.append(x)
					break
		ultimo -= 1					
	return recomendables
				
def recomendar(grafo,personaje):
	dic = {}
	caminos = caminos=caminos_random(grafo,personaje)
	frec =frecuencias(caminos,dic)								
	recomendables = a_recomendar(grafo,frec,dic,personaje)
	for personaje in recomendables:
		print(grafo.vertices[personaje][0])

def distancias(grafo,personaje):
	distancias = grafo.bfs(personaje)
	dic={}
	for x in distancias:
		freq = dic.get(distancias[x],0)
		freq+=1
		dic[distancias[x]] = freq
	distancia = list(dic.keys())
	distancia.sort()
	for x in distancia:
		print( "Distancia" + str(x) + "= " + str(dic[x]))
		
			 
def imprimir_comunidad(comunidad,grafo):#lista
	"""Dado una comunidad (respresentada por una lista) imprime cada
	integrante de la misma"""
	print("Cantidad de miembros = " + str(len(comunidad)))
	for personaje in comunidad:
		print (grafo.vertices[personaje][0])

def comunidades(grafo):
	comunidades = grafo.label_propagation()
	for label in comunidades:
		comunidad = comunidades[label]
		if len(comunidad) > 4 and len(comunidad) < 1000:
			imprimir_comunidad(comunidad,grafo)


def camino(grafo,personaje1,personaje2):
	dist , camino = grafo.dijkstra(personaje1)
	recorrido = [personaje2]
	padre = camino[personaje2]
	while padre != personaje1:
		recorrido.append(padre)
		padre = camino[padre]
	recorrido.append(padre)
	for i in range (len(recorrido)-1,-1,-1):
		print(grafo.vertices[recorrido[i]][0])	
	
def centralidad(grafo):#no funcaaaa
	cent = {}
	for vertice in grafo.vertices: 
		cent[vertice] = 0
	for vertice in grafo.vertices:
		for vertice2 in grafo.vertices:
			if vertice == vertice2:
				continue
			distancia , padre = grafo.dijkstra(vertice)
			if padre[vertice2] == None:
				continue
			actual = padre[vertice2]
			while actual != vertice:
				cent[actual] +=1
				actual= padre[actual]
	mayores = cent.values()
	mayores.sort()			
	max1 = mayores[len(mayores)-1]
	max2 = mayores[len(mayores)-2]
	max3 = mayores[len(mayores)-3]
	for x in cent:
		if cent[x] == max1 or cent[x] == max2 or cent[x] == max3:
			print (x)
def estadisticas(grafo):
	print("Numero de vertices : " + str(len(grafo)))
	print("Numero de aristas : " + str(grafo.len_aristas()))
	print("Densidad del grafo : " + str(grafo.densidad()))
	print("Promedio del grado de cada vertice : " + str(grafo.promedio_del_grado()))

menu()
